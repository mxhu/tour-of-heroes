import { Component } from '@angular/core';
import { Hero} from './hero';
import { HeroService } from './hero.service';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'my-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: [ './heroes.component.css' ]

})


export class HeroesComponent implements OnInit{
  heroes: Hero[]; // an uninitialized hereos property
  selectedHero: Hero;

  // Now Angular knows to supply an instance of the HeroService when it creates an AppComponent
  // To teach how to make a HeroService, add HeroService in providers array of the component metadata in the @Component call.
  constructor(
    private heroService: HeroService,
    private router: Router
  ) { }

  getHeroes(): void {
    // the arrow function "heroes => this.heroes = heroes" equals to
    // func(heroes: Hero[]): void {
    //   this.heroes = heroes; 
    // } 
    this.heroService.getHeroesSlowly().then(heroes => this.heroes = heroes);
  }

  ngOnInit(): void {
    this.getHeroes();
  }

  /*
  hero: Hero = {
    id: 1,
    name: 'Windstorm'
  };
  //*/
  
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) return;
    this.heroService.create(name)
                    .then(hero => {
                      this.heroes.push(hero);
                      this.selectedHero = null;
                    });
  }

  delete(hero: Hero): void {
    this.heroService
        .delete(hero.id)
        .then(() => {
          this.heroes = this.heroes.filter(h => h != hero);
          if (this.selectedHero === hero) this.selectedHero = null;
        });
  }
}

