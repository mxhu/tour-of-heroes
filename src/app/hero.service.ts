import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Hero } from './hero';
import { HEROES } from './mock-heroes'

@Injectable()
export class HeroService {
  private heroesUrl = 'api/heroes'; // URL to web api
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  getHeroesOldVersion(): Promise<Hero[]> { //  The HeroService get data from local storage, or a mock data source.
    return Promise.resolve(HEROES);
  }

  getHeroes(): Promise<Hero[]> { //  The HeroService get data from anywhere—a web service.
    return this.http.get(this.heroesUrl)
                    // The Angular http.get returns an RxJS Observable, converted the Observable to a Promise.
                    // however, A request-cancel-new-request sequence is difficult to implement with Promises, 
                    // but easy with Observables.
                    .toPromise() 
                    // you call the json method of the HTTP Response to extract the data within the response.
                    .then(response => response.json().data as Hero[]) 
                    .catch(this.handleError);
  }

  getHeroesSlowly(): Promise<Hero[]> {
    return new Promise(resolve => {
      // delay 0.5 sec
      setTimeout(() => resolve(this.getHeroes()), 500);
    });
  }


  getHeroOldVersion(id: number): Promise<Hero> {
    return this.getHeroes().then(heroes => heroes.find(hero => hero.id === id));
  }

  // Most web APIs support a get-by-id request in the form api/hero/:id (such as api/hero/11).
  getHero(id: number): Promise<Hero> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get(url)
               .toPromise()
               .then(response => response.json().data as Hero)
               .catch(this.handleError);
  }
  
  update(hero: Hero): Promise<Hero> {
    const url = `${this.heroesUrl}/${hero.id}`;
    // The put() body is the JSON string encoding of the hero, 
    // obtained by calling JSON.stringify. The body content 
    // type (application/json) is identified in the request header.
    return this.http.put(url, JSON.stringify(hero), {headers: this.headers})
                    .toPromise()
                    .then(() => hero)
                    .catch(this.handleError);
  }

  create(name: string): Promise<Hero> {
    return this.http.post(this.heroesUrl, JSON.stringify({name: name}), {headers: this.headers})
                    .toPromise()
                    .then(res => res.json().data as Hero)
                    .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
                    .toPromise()
                    .then(() => null)
                    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    // This demo service logs the error to the console;
    // in real life, you would handle the error in code.
    console.error('An error occurred ', error);
    return Promise.reject(error.message || error);
  }

  //
}